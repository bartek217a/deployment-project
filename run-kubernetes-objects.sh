#!/bin/bash

kubectl apply -f config."$ENVIRONMENT_NAME".yml

kubectl apply -f configuration-microservice/deployment.yml
kubectl apply -f configuration-microservice/service.yml

kubectl wait deployment -n default configuration-microservice-deployment --for condition=Available=True --timeout=600s

kubectl apply -f child-microservice/deployment.yml
kubectl apply -f child-microservice/service.yml

kubectl wait deployment -n default child-microservice-deployment --for condition=Available=True --timeout=600s

kubectl apply -f parent-microservice/deployment.yml
kubectl apply -f parent-microservice/service.yml

kubectl wait deployment -n default parent-microservice-deployment --for condition=Available=True --timeout=600s
