Creating objects:
kubectl apply -f ./configuration-microservice-pod.yml

List objects by type:
kubectl get pods
kubectl get services
kubectl get deployment

Kubernetes cluster ssh
minikube ssh

Show logs of container X from pod Y
kubectl logs Y X+

Open console in container X of pod Y
kubectl exec -it Y --container X -- sh

Add curl to alpine container
apk add curl

Restart deployment X
kubectl rollout restart deployment/X

Start minikube cluster
minikube start --vm-driver kvm2

Start AWS EKS cluster
eksctl create cluster -f eks-cluster.yml

eksctl config schema
https://eksctl.io/usage/schema/
